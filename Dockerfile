FROM golang:latest as builder

WORKDIR /home/app

COPY . .

RUN go env -w GO111MODULE=on \
    && go env -w GOPROXY=https://goproxy.cn,direct \
    && go env -w CGO_ENABLED=0 \
    && go env \
    && go mod tidy \
    && GOOS=linux go build -o ./main

FROM alpine:3

COPY --from=builder /home/app/main .

EXPOSE 8080

ENTRYPOINT ./main
